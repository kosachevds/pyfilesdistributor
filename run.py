import os
import math
from distutils import file_util

_SOURCE_DIR = r"D:\music\Phone"
_DESTINATION_DIR = r"D:\music\Phone2"
_MAX_DIR_ITEMS = 32
# _MAX_DIRS_COUNT =


def main():
    files = _get_all_filenames(_SOURCE_DIR)
    files_count = len(files)
    dirnames = _get_dirnames(files_count)
    for i, fname in enumerate(files):
        dirname = dirnames[i % len(dirnames)]
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        file_util.copy_file(fname, dirname)
    print("Done!")


def _get_all_filenames_with_childs(rootpath: str) -> list:
    result = []
    for dirname, _, files in os.walk(rootpath):
        for fname in files:
            result.append(os.path.join(dirname, fname))
    return result


def _get_all_filenames(rootpath: str) -> list:
    all_files = [os.path.join(rootpath, x) for x in os.listdir(rootpath)]
    return list(filter(os.path.isfile, all_files))


def _get_dirnames(files_count):
    dir_count = math.ceil(files_count / _MAX_DIR_ITEMS)
    dirnames = map(str, range(1, dir_count + 1))
    return [os.path.join(_DESTINATION_DIR, str(x)) for x in dirnames]


if __name__ == "__main__":
    main()
